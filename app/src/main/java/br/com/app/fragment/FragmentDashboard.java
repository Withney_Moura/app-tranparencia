package br.com.app.fragment;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import com.tranparencia.R;
import br.com.app.adapter.ObrigacaoListViewAdapter;
import br.com.app.model.Obrigacao;
import br.com.app.service.ApiFactory;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.animation.AnimationUtils.loadAnimation;
import static android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH;
import static br.com.app.util.AndroidSystemUtil.hideSoftKeyboard;
import static br.com.app.util.StringUtil.formatarMoeda;
import static com.daimajia.androidanimations.library.Techniques.*;
import static com.daimajia.androidanimations.library.YoYo.*;

public class FragmentDashboard extends Fragment implements SwipeRefreshLayout.OnRefreshListener, AdapterView.OnItemClickListener{

    @BindView(R.id.debitoList)
    ListView obrigacoesListView;

    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    br.com.app.service.AppService service;
    ObrigacaoListViewAdapter accountListViewAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        service = ApiFactory.api(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);
        ButterKnife.bind(this, view);
        swipeRefreshLayoutConfig();
        obrigacoesListView.setOnItemClickListener(this);
        obrigacoesListView.setTextFilterEnabled(true);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getDebitos();
    }

    private void swipeRefreshLayoutConfig(){
        swipeRefreshLayout.setColorSchemeResources(R.color.white);
        swipeRefreshLayout.setProgressBackgroundColorSchemeResource(R.color.dark_blue_marine);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void onRefresh() {
        getDebitos();
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

    }

    private void getDebitos(){
        Call<List<Obrigacao>> call = service.getAllObrigacoes();
        call.enqueue(new Callback<List<Obrigacao>>() {

            @Override
            public void onResponse(Call<List<Obrigacao>> call, Response<List<Obrigacao>> response) {
                int statusCode = response.code();
                if (statusCode == 200) {
                    List<Obrigacao> obrigacoes = response.body();

                    Obrigacao obrigacao = new Obrigacao();
                    if (!obrigacoes.isEmpty()) {
                        obrigacao = obrigacoes.get(0);
                    }

                    accountListViewAdapter = new ObrigacaoListViewAdapter(getActivity(), obrigacoes);
                    obrigacoesListView.setAdapter(accountListViewAdapter);
                    accountListViewAdapter.notifyDataSetChanged();
                    swipeRefreshLayout.setRefreshing(false);
                    with(SlideInUp).duration(500).playOn(obrigacoesListView);

                    BigDecimal valorTotal = getValorTotal(obrigacoes);
                    setListViewHeader(obrigacao, valorTotal);
                }
            }

            @Override
            public void onFailure(Call<List<Obrigacao>> call, Throwable t) {
                swipeRefreshLayout.setRefreshing(false);
            }

        });
    }

    private BigDecimal getValorTotal(List<Obrigacao> obrigacoes) {
        BigDecimal valorTotal = BigDecimal.ZERO;
        for (Obrigacao obrigacao : obrigacoes) {
            BigDecimal valor = obrigacao.getValor();
            valorTotal = valorTotal.add(valor);
        }
        return valorTotal;
    }

    @TargetApi(Build.VERSION_CODES.N)
    private void setListViewHeader(Obrigacao obrigacao, BigDecimal valorTotal) {
        if(obrigacoesListView.getHeaderViewsCount() == 1){
            obrigacoesListView.removeHeaderView(obrigacoesListView.findViewWithTag("listviewheader"));
        }

        View headerView = getActivity().getLayoutInflater().inflate(R.layout.header_valores, obrigacoesListView, false);
        headerView.setTag("listviewheader");

        TextView valorTotalTextView = (TextView) headerView.findViewById(R.id.valor_total);
        valorTotalTextView.setText("Total: " + formatarMoeda(valorTotal.toString()));

        TextView valorObrigacaoMesTextView = (TextView) headerView.findViewById(R.id.valor_obrigacao_mes);
        valorObrigacaoMesTextView.setText(formatarMoeda(obrigacao.getValor().toString()));

        TextView mesObrigacaoTextView = (TextView) headerView.findViewById(R.id.mes_obrigacao);
        mesObrigacaoTextView.setText(obrigacao.getDataObrigacao());

        EditText inputSearch = (EditText) headerView.findViewById(R.id.inputSearch);
        onEditorActionListener(inputSearch);


        obrigacoesListView.addHeaderView(headerView, null, false);

        Animation animationStart = loadAnimation(getActivity(), android.R.anim.slide_in_left);
        animationStart.setDuration(1000);
        headerView.startAnimation(animationStart);
    }

    private void onEditorActionListener(EditText inputSearch) {
        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
                if (actionId == IME_ACTION_SEARCH) {
                    hideSoftKeyboard(getActivity());
                    accountListViewAdapter.getFilter().filter(textView.getText());
                    return true;
                }
                return false;
            }
        });
    }
}
