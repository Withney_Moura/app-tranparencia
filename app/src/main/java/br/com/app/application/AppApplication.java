package br.com.app.application;

import android.app.Application;
import android.content.Context;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class AppApplication extends Application {

    public static final String URL_BASE = "https://private-336a3-withney.apiary-mock.com/";

    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        AppApplication.context = getApplicationContext();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/OpenSans-Light.ttf")
                .setFontAttrId(uk.co.chrisjenx.calligraphy.R.attr.fontPath)
                .build()
        );
    }

    public static Context getAppContext() {
        return AppApplication.context;
    }
}
