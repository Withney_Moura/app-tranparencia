package br.com.app.util;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

import com.tranparencia.R;
import br.com.app.application.AppApplication;

public class SnackBarUtil {

    public static void showSnackBar(View view, int messageRes, Context context){
        Snackbar snackbar = Snackbar.make(view, messageRes, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        snackbar.show();
    }

    public static void showSnackBarError(View view, int messageRes){
        Snackbar snackbar = Snackbar.make(view, messageRes, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(AppApplication.getAppContext().getResources().getColor(android.R.color.holo_red_light));
        snackbar.show();
    }

    public static void showSnackBarError(View view, String message){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(AppApplication.getAppContext().getResources().getColor(android.R.color.holo_red_light));
        snackbar.show();
    }
}