package br.com.app.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtil {

    public static String getToday() {
        DateFormat dfmt = new SimpleDateFormat("EEEE, d 'de' MMMM 'de' yyyy");
        Date hoje = Calendar.getInstance(Locale.getDefault()).getTime();
        return dfmt.format(hoje);
    }

    public static String getDateTime(Date date) {
        DateFormat dfmt = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
        return dfmt.format(date);
    }

    public static String getDate(Date date) {
        DateFormat dfmt = new SimpleDateFormat("dd/MM/yy");
        return dfmt.format(date);
    }
}
