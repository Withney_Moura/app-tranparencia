package br.com.app.util;

import android.widget.EditText;

import java.text.DecimalFormat;
import java.util.Currency;

public class StringUtil {

    public static String formatarMoeda(String texto) {
        DecimalFormat formatoMoeda = new DecimalFormat("#,##0.00");
        formatoMoeda.setCurrency(Currency.getInstance("BRL"));
        try {
            String formatado = formatoMoeda.format(Double.valueOf(texto)).replace("R$", "").replace(" ", ".").replace(" ", ".").trim();
            return "R$ " + formatado;
        } catch (Exception e) {
            return texto;
        }
    }

    public static boolean isEditTextNullOrBlank(EditText editText) {
        return editText.getText().toString() == null || editText.getText().toString().isEmpty();
    }
}
