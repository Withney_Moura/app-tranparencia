package br.com.app.service;

import java.util.List;

import br.com.app.model.Obrigacao;
import br.com.app.model.Token;
import br.com.app.model.Usuario;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface AppService {

    @POST("api/authenticate")
    Call<Token> getLogin(@Body Usuario usuario);

    @GET("api/obrigacoes")
    Call<List<Obrigacao>> getAllObrigacoes();
}
