package br.com.app.service;

import android.content.Context;

import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

import br.com.app.application.AppApplication;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static br.com.app.preferences.AppPreferences.*;

public class ApiFactory {

    public static AppService api() {
        return create(AppService.class);
    }

    public static AppService api(Context context) {
        return create(AppService.class, context);
    }

    public static AppService apiWithTimeout(Context context) {
        return createWithTimeout(AppService.class, context);
    }

    private static <T> T create(Class<T> endpoint, final Context context) {

        String token = "Bearer " + loadValuePreferences(context, ACCESS_TOKEN);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addNetworkInterceptor(new AddHeaderInterceptor(token));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppApplication.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit.create(endpoint);
    }

    private static <T> T create(Class<T> endpoint) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppApplication.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create(new Gson()))
                .build();

        return retrofit.create(endpoint);
    }

    private static <T> T createWithTimeout(Class<T> endpoint, final Context context) {

        String token = "Bearer " + loadValuePreferences(context, ACCESS_TOKEN);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addNetworkInterceptor(new AddHeaderInterceptor(token));
        httpClient.connectTimeout(60, TimeUnit.SECONDS);
        httpClient.readTimeout(60, TimeUnit.SECONDS);
        httpClient.writeTimeout(60, TimeUnit.SECONDS);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppApplication.URL_BASE)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClient.build())
                .build();

        return retrofit.create(endpoint);
    }

}