package br.com.app.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tranparencia.R;
import br.com.app.fragment.FragmentDashboard;
import br.com.app.model.Menu;
import br.com.app.slidingmenu.SlidingMenuAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

import static android.view.LayoutInflater.from;
import static android.widget.AdapterView.OnItemClickListener;
import static br.com.app.model.Menu.generateMenuStub;
import static br.com.app.preferences.AppPreferences.*;
import static br.com.app.util.SnackBarUtil.showSnackBar;
import static org.parceler.apache.commons.lang.StringUtils.*;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout mDrawerLayout;
    @BindView(R.id.navList) ListView mDrawerList;
    @BindView(R.id.main_view_pager) RelativeLayout relativeLayout;

    private ActionBarDrawerToggle mDrawerToggle;
    private View drawerHeader;
    private SlidingMenuAdapter mAdapter;
    private Fragment fragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setupToolbar();
        setupDrawer();
        initialFragment();

        showSnackBar(relativeLayout, R.string.welcome, this);
    }

    private void initialFragment() {
        fragment = new FragmentDashboard();
        beginTransactionFragment();
    }

    private void beginTransactionFragment() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
    }

    private void setupDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                invalidateOptionsMenu();
            }

        };
        mDrawerToggle.syncState();
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        drawerHeader = from(MainActivity.this).inflate(R.layout.header_sliding_menu, null);
        mDrawerList.addHeaderView(drawerHeader);
        addUserNameMenu();

        mAdapter = new SlidingMenuAdapter(this, generateMenuStub(getApplicationContext()));
        mDrawerList.setAdapter(mAdapter);

        mDrawerList.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Resources resources = getResources();
                Menu menu = (Menu) parent.getAdapter().getItem(position);
                if(menu != null){
                    /*if(menu.title.equals(resources.getString(R.string.logout))){
                        removePreferences(getApplicationContext());
                        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }*/
                    beginTransactionFragment();
                }
                mDrawerLayout.closeDrawers();
            }
        });
    }

    private void addUserNameMenu() {
        TextView textView = (TextView) drawerHeader.findViewById(R.id.name_user);
        String nameUser = loadValuePreferences(getApplicationContext(), NAME_USER);
        textView.setText(upperCase(nameUser));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
