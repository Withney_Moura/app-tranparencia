package br.com.app.slidingmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.tranparencia.R;
import br.com.app.model.Menu;
import butterknife.BindView;

import java.util.List;

public class SlidingMenuAdapter extends ArrayAdapter<Menu> {

    public SlidingMenuAdapter(Context context, List<Menu> items) {
        super(context, 0, items);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sliding_menu, parent, false);

            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.item_sliding_menu_textview_title);
            holder.icon = (ImageView) convertView.findViewById(R.id.item_sliding_menu_imageview_icon);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.icon.setImageResource(getItem(position).icon);
        holder.title.setText(getItem(position).title);

        return convertView;
    }

    private class ViewHolder {
        public TextView title;
        public ImageView icon;
    }
}