package br.com.app.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import com.tranparencia.R;
import br.com.app.model.Obrigacao;
import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.LayoutInflater.from;
import static br.com.app.util.StringUtil.formatarMoeda;

public class ObrigacaoListViewAdapter extends ArrayAdapter<Obrigacao> {

    @BindView(R.id.valor_obrigacao) TextView valorTextView;
    @BindView(R.id.data_obrigacao) TextView dateTextView;
    @BindView(R.id.descricao) TextView descricaoTextView;

    Context context;

    public ObrigacaoListViewAdapter(Context context, List<Obrigacao> objects) {
        super(context, 0, 0, objects);
        removeMesAtual(objects);
        this.context = context;
    }

    private void removeMesAtual(List<Obrigacao> objects) {
        if (!objects.isEmpty()) {
            objects.remove(0);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = from(getContext()).inflate(R.layout.list_item_obrigacao, null);
        ButterKnife.bind(this, convertView);

        final Obrigacao obrigacao = getItem(position);
        valorTextView.setText(formatarMoeda(obrigacao.getValor().toString()));
        dateTextView.setText(obrigacao.getDataObrigacao());
        descricaoTextView.setText(obrigacao.getDescricao());
        return convertView;
    }
}
