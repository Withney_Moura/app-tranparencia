package br.com.app.model;

import android.content.Context;
import android.content.res.Resources;

import java.util.ArrayList;
import java.util.List;

import com.tranparencia.R;

public class Menu {

    public int icon;
    public String title;

    Menu(int icon, String title) {
        this.icon = icon;
        this.title = title;
    }

    public static List<Menu> generateMenuStub(Context context) {
        Resources resources = context.getResources();
        List<Menu> items = new ArrayList<>();
        items.add(new Menu(R.drawable.ic_account_switch, resources.getString(R.string.swap_user)));
        items.add(new Menu(R.drawable.ic_logout, resources.getString(R.string.logout)));
        return items;
    }
}
