package br.com.app.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Obrigacao {

    @SerializedName("valor")
    private BigDecimal valor = BigDecimal.ZERO;

    @SerializedName("data")
    private String dataObrigacao;

    @SerializedName("descricao")
    private String descricao;

    public BigDecimal getValor() {
        return valor;
    }

    public void setValor(BigDecimal valor) {
        this.valor = valor;
    }

    public String getDataObrigacao() {
        return dataObrigacao;
    }

    public void setDataObrigacao(String dataObrigacao) {
        this.dataObrigacao = dataObrigacao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
