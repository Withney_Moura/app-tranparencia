package br.com.app.preferences;

import android.content.Context;
import android.content.SharedPreferences;

public class AppPreferences {

    public static final String SHARED_PREFS = "AppPreferences";
    public static final String ACCESS_TOKEN = "id_token";
    public static final String NAME_USER = "name_user";

    public static void saveValuePreferences(Context context, String key, String value) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, 0);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String loadValuePreferences(Context context, String key) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, 0);
        return sharedPreferences.getString(key, "");
    }

    public static void removePreferences(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFS, 0);
        sharedPreferences.edit().clear().commit();
    }
}
